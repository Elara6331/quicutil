# quicutil

quicutil is a simple package that provides convenience functions that generate self-signed certificates for QUIC connections that don't require PKI, such as trusted connections over a local network, or connections where a different verification mechanism is used.

Connections and servers created by this package are encrypted but not secure. They may be vulnerable to MitM attacks if a separate verification mechanism isn't used.

**Do not use this in production for applications where PKI is needed, such as for public websites!**